Howdy!


This is a repository containing code written and designed for a motorized wheelchair. In this project 
two Pulse Width Modulated (PWM) signals along with some control bits are sent to motor control hardware. 
This is the software containing the following:

* Motor Controllers - Abstractions behind low level microcontroller PWM
* MCAIDev - Abstraction over the input device (joystick) used to get and set data
* Finite State Machine - 5 State Finite State Machine responsible for controlling motor controllers.
* RF24 Lib - This is a C port of the Arduino library.
* Others - Implementation details for radio communication, low level abstractions, and other useful things.  