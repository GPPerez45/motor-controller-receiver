/*
 * pwmpfc.hpp
 * Pulse Width Modulation Frequency and Phase Correct
 *
 */


#ifndef PWMPFC_HPP_
#define PWMPFC_HPP_

#include "pmmio.hpp"

class PWMPFC : private PMMIO
{
  private:
    volatile uint16_t * OCR;       // PWM duty cycle reg pointer

    unsigned long PWM_Max_DC;      // Maximum PWM duty cycle
    unsigned int PWM_Shift_DC;     // Shift PWM duty cycle for %

  private:
    unsigned long GetOCRDutyCycle( void );
    void SetOCRDutyCycle( unsigned long );

  public:
    unsigned int GetDutyCycle( void );   // returns % duty cycle (0-100)
    void SetDutyCycle( unsigned int );   // set % duty cycle (0-100)

    PWMPFC( volatile uint16_t *ocr, unsigned long const max_dc, unsigned int const shift_dc )
    {
      OCR = ocr;
      PWM_Max_DC = max_dc;
      PWM_Shift_DC = shift_dc;
    };

};

#endif // PWMPFC_HPP_

