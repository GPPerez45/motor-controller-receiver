/*
 * timerbase.hpp
 *
 * Base class for timers. 
 */

#ifndef TIMERBASE_HPP_
#define TIMERBASE_HPP_

//=====================================================
// Calculate 16bit MilSec Timer
//=====================================================
// ATMega128
// Fclk = 16MHz
// Timer Frequency = 1 ms
//
// The folowing is from Dean Camera :
// AVR Freaks : Tutorial on Timers
//
// TTC  = Target Timer Count
// TF   = Target Frequency
// TCF  = Timer Clock Frequency
// TCFP = Timer Clock Frequency Prescaler
//
// TTC  = ?
// TF   = 1 ms
// TCF  = 16Mhz
// TCFP = 1
//
// TTC = (1/TF) / (TCFP/TCF) - 1
// TTC = (1/1000) / (1/16000000) - 1
// TTC = .001/.0000000625 - 1
// TTC = 15999
//
// TMR_MILSEC_TIMER_ = 65535 - TCC
// TMR_MILSEC_TIMER_ = 49536
//
// ---------------------------------
//

#include "objtimers.hpp"
#include "syserror.hpp"

namespace TMR16BITOI
{
  static unsigned long const MILSEC_TIMER = 49536;
}

namespace TMR8BITOI
{
  static unsigned int const MILSEC_TIMER = 6;
}

class TimerBase: public SystemError
{
  private:
    static unsigned int const OBJPTR_SIZE = 20;

    ObjTimers * ObjPtr[ OBJPTR_SIZE ];

  protected:
    TimerBase( void );
    void UpdateObjTimers( void );

  public:
    unsigned int AssignObject( class ObjTimers * );
    unsigned int RemoveObject( class ObjTimers * );
};

#endif // TIMERBASE_HPP_

