
/*
  The following code was taken from AVR Freaks GCC forum distributed by TFrancuz
  http://www.avrfreaks.net/index.php?name=PNphpBB2&file=viewtopic&p=457778
  The new and delete operators for C++
*/

#include "new.hpp"

void * operator new(size_t size)
{
  return malloc(size);
}

void * operator new[](size_t size)
{
  return malloc(size);
}

void operator delete(void * ptr)
{
  free(ptr);
}

void operator delete[](void * ptr)
{
  free(ptr);
}

int __cxa_guard_acquire(__guard *g) {return !*(char *)(g);};
void __cxa_guard_release (__guard *g) {*(char *)g = 1;};
void __cxa_guard_abort (__guard *) {};

void __cxa_pure_virtual(void) {};

