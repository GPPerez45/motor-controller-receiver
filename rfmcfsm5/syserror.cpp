/*
 * syserror.cpp
 *
 * Infinite Loop In Event of Error
 */

#include <avr/io.h>

#include "syserror.hpp"

void SystemError::SysError( unsigned int error_num = 0 )
{
  PORTC = 0x00;
  PORTD = 0x00;
  PORTJ = 0x00;
  PORTH = 0x00;
  TCCR4A = 0x00;
  while( 1 );
  return;
}
