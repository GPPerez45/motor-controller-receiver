/*
 * jsmcfsm5.hpp
 *
 * Joystick Motor Control 5 State Finite State Machine
 *
 */

#ifndef JSMCFSM5_HPP_
#define JSMCFSM5_HPP_

#include "mc.hpp"
#include "mcaidev.hpp"


class JSMCFSM5
{
  private:
    static uint8_t const MFS    = 100;   // Max Fwd Speed
    static uint8_t const MRS    =  70;   // Max Rev Speed
    static uint8_t const MSS    =  12;   // Min Motor 1 & 2 Start Speed
    static uint8_t const FP_TS  =  60;   // Fix Pos Turn Speed
    static uint8_t const FR_TS  =  20;   // Fwd-Rev Turn Speed
    static uint8_t const FR_RUR =  15;   // Fwd-Rev Ramp Up Rate
    static uint8_t const FR_RDR =   5;   // Fwd-Rev Ramp Down Rate
    static uint8_t const FR_SRR =   2;   // Fwd-Rev Start Ramp Rate
    static uint8_t const FR_RTR =  15;   // Fwd-Rev Ramp Turn Rate
    static uint8_t const FR_RRR =  15;   // Fwd-Rev Ramp Return Rate
    static uint8_t const FP_RTR =  20;   // Fix Pos Ramp Turn Rate
    static uint8_t const FP_RRR =   5;   // Fix Pos Ramp Return Rate

    static uint8_t const MTR_DIR_REV   = 0x0A;  //  b00001010
    static uint8_t const MTR_DIR_FWD   = 0x05;  //  b00000101
    static uint8_t const MTR_DIR_OPEN  = 0x00;  //  b00000000
    static uint8_t const MTR_DIR_RIGHT = 0x09;  //  b00001001
    static uint8_t const MTR_DIR_LEFT  = 0x06;  //  b00000110

    static uint8_t const MTR1_DIR_REV   = 0x02;  //
    static uint8_t const MTR2_DIR_REV   = 0x08;  //
    static uint8_t const MTR1_DIR_FWD   = 0x01;  //
    static uint8_t const MTR2_DIR_FWD   = 0x04;  //
//    static uint8_t const MTR1_DIR_RIGHT = 0x01;  //
//    static uint8_t const MTR2_DIR_RIGHT = 0x08;  //
//    static uint8_t const MTR1_DIR_LEFT  = 0x02;  //
//    static uint8_t const MTR2_DIR_LEFT  = 0x04;  //

    static uint8_t const FWD  = 4;       // Motor Foeward
    static uint8_t const REV  = 3;       // Motor Reverse
    static uint8_t const FPLT = 2;       // Motor Fixed Position Left Turn
    static uint8_t const FPRT = 1;       // Motor Fixed Position Right Turn
    static uint8_t const DZ   = 0;       // Motor Dead Zone

  private:
    void SyncMotors( class MotorController *, class MotorController * );
    uint8_t IsMSSStopped( class MotorController *, class MotorController * );
//    uint8_t IsMtrDirSync( class MotorController *, class MotorController *, uint8_t );
//    uint8_t GetMtrDir( class MotorController *, class MotorController * );
    void SetMtrRampRate( class MotorController *, class MotorController *, uint8_t );
    void SetMtrRampDown( class MotorController * );
    void SetMotorsRampDown( class MotorController *, class MotorController * );
    void SetMtrRev( class MotorController *, class MotorController *, class MCAIDev * );
    void SetMtrFPLftTurn( class MotorController *, class MotorController *, class MCAIDev * );
    void SetMtrFPRgtTurn( class MotorController *, class MotorController *, class MCAIDev * );
    void SetMtrFwd( class MotorController *, class MotorController *, class MCAIDev * );

    //================= 5 State Finite State Machine ==================

    void MachState0( class MotorController *, class MotorController *, class MCAIDev * );
    void MachState1( class MotorController *, class MotorController *, class MCAIDev * );
    void MachState2( class MotorController *, class MotorController *, class MCAIDev * );
    void MachState3( class MotorController *, class MotorController *, class MCAIDev * );
    void MachState4( class MotorController *, class MotorController *, class MCAIDev * );
    void FSM5State(  class MotorController *, class MotorController *, class MCAIDev * );

  public:
    JSMCFSM5( void )
    {
    };

  public:
    uint8_t GetMtrDir( class MotorController *, class MotorController * );
    void ExecFSM5State( class MotorController *, class MotorController *, class MCAIDev * );
};

#endif /* JSMCFSM5_HPP_ */

