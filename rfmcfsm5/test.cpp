#include <avr/io.h>
#include <avr/interrupt.h>
#define F_CPU 16000000UL
#include <util/delay.h>

#include "timer2oi.hpp"
#include "mcaidev.hpp"
#include "jsmcfsm5.hpp"

// Transciever stuff
#include "spi.hpp"
#include "nRF24L01.h"
#include "RF24.h"

#define CE_PIN  0x10                  // Chip Enable
#define CSN_PIN 0x20                  // Chip Select (Arduino 2560)
const uint64_t pipe = 0xE8E8F0F0E1LL; // Define the transmit pipe

#include "pwmpfc.hpp"
#include "pwm4pfc.hpp"
#include "pwm4pfcPH3.hpp"
#include "pwm4pfcPH4.hpp"
#include "pwm4pfcPH5.hpp"
#include "mc.hpp"
#include "mc1.hpp"
#include "mc2.hpp"
#include "mc3.hpp"




int main( void )
{
  unsigned int aidev;

  Timer2OI timer2;

  PWM4PFCPH3 pwm4PH3;
  PWM4PFCPH4 pwm4PH4;
  PWM4PFCPH5 pwm4PH5;

  MC1 mc1( &timer2, &pwm4PH3 );
  MC2 mc2( &timer2, &pwm4PH4 );

  MCAIDev mcaidev;
  JSMCFSM5 jsmcfsm;

  sei(); // Enable Global Interrupt for System Timer, PWM Timer

  SPIClass spi;
  RF24 radio( &PORTB, &DDRB, CE_PIN, &PORTB, &DDRB, CSN_PIN, &spi );  //  Declare RF24 obj instance for nRF24L01+

  radio.begin();
  radio.openReadingPipe(1,pipe);
  radio.startListening();

  mcaidev.SetM1Speed( 0 );
  mcaidev.SetM2Speed( 0 );
  mcaidev.SetMDir( 0 );

  DDRC = 0xFF;
  DDRD = 0xFF;
  DDRJ = 0xFF;
  DDRH = 0xFF;

  timer2.StartTimer( T2OI::T2OI0, 5000 );

  while( true )
  {
    timer2.UpdateTimers();

    aidev = mcaidev.Driver( &radio );
    if( aidev == false )
    {
      if( timer2.TimerDone( T2OI::T2OI0 ) == true )
      {
        PORTJ = 0XFF;
      }
    }
    else
    {
      timer2.StartTimer( T2OI::T2OI0, 5000 );

      jsmcfsm.ExecFSM5State( &mc1, &mc2, &mcaidev );
      mc1.MCDriver();
      mc2.MCDriver();
	  uint8_t speed1, speed2, dir;
	  speed1 = mcaidev.GetM1Speed();
	  speed2 = mcaidev.GetM2Speed();
      if( mcaidev.IsKillSwitchActive() == true ) {
		SystemError();
	  }
    }
  }
}

