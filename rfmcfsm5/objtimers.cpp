/*
 * objtimers.cpp
 *
 * Object Timer Implementation. 
 */

//#include<stdlib.h>

#include "new.hpp"
#include "timerbase.hpp"
#include "objtimers.hpp"
#include "syserror.hpp"

ObjTimers::ObjTimers( class TimerBase * t_ptr, unsigned int const t_num )
{
  if( t_num == 0 ) SysError( 0 );
  if( t_ptr == 0 ) SysError( 0 );

  ObjectTimers = new unsigned long [ t_num ];
  if( ObjectTimers == 0 ) SysError( 0 );

  for( unsigned int i = 0; i < t_num; ++i )
    ObjectTimers[i] = 0;

  TimerNum = t_num;
  TimerBasePtr = t_ptr;
  TimerBasePtr->AssignObject( this );
}

ObjTimers::~ObjTimers( void )
{
  if( TimerBasePtr == 0 ) SysError( 0 );

  TimerBasePtr->RemoveObject( this );
  if( ObjectTimers != 0 ) delete ObjectTimers;
}

//Initialize timer with value
void ObjTimers::StartTimer( unsigned int const t_indx, unsigned long t_time )
{
  if( t_indx >= TimerNum ) SysError( 0 );
  if( ObjectTimers == 0 ) SysError( 0 );

  ObjectTimers[t_indx] = t_time;
}

//Check if the timer is done.
unsigned int ObjTimers::TimerDone( unsigned int const t_indx )
{
  if( t_indx >= TimerNum ) SysError( 0 );
  if( ObjectTimers == 0 ) SysError( 0 );

  if( ObjectTimers[t_indx] == 0 )
    return true;

  return false;
}

//Decrement Timer Values
void ObjTimers::UpdateTimers( void )
{
  if( ObjectTimers == 0 ) SysError( 0 );

  for( unsigned int i = 0; i < TimerNum; ++i )
    if( ObjectTimers[i] != 0 )
      --ObjectTimers[i];
}

