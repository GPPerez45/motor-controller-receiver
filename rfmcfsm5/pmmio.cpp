/*
 * pmmio.cpp
 *
 * Pointer to Memory Mapped IO port Routines
 *
 */

#include "pmmio.hpp"

void PMMIO::SetPortBits( volatile uint8_t *port, uint8_t bits, uint8_t mask )
{
  *((volatile uint8_t *) port) = (*((volatile uint8_t *) port) & (mask ^ 0xFF)) | bits;
}


unsigned int PMMIO::GetPortBits( volatile uint8_t *port, uint8_t mask )
{
  return *((volatile uint8_t *) port) & mask;
}


void PMMIO::Set16BitPort( volatile uint16_t *port, uint16_t val )
{
  *(volatile unsigned int *) port = val;
}


unsigned long PMMIO::Get16BitPort( volatile uint16_t *port )
{
  return *(volatile unsigned int *) port;
}


