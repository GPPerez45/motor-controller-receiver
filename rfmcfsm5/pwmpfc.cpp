/*
 * pwmpfc.cpp
 * Pulse Width Modulation Frequency and Phase Correct
 *
 */

#include "pwmpfc.hpp"
#include "syserror.hpp"


/*
	Method Name: GetOCRDutyCycle
	Parameter(s): None
	Description: Returns a 32-bit integer representing the duty cycle. 
*/

unsigned long PWMPFC::GetOCRDutyCycle( void )
{
  unsigned long dc;

  dc = Get16BitPort( OCR );

  if( dc > PWM_Max_DC ) SystemError(); // duty cycle out of bounds

  return dc;
}

/*
	Method Name: SetOCRDutyCycle
	Parameters: A 32-bit unsigned integer representing the duty cycle.
	Description: Sets the duty cycle within the range [0 - 100]. If it's out of range,
	then it gives an error.  
*/


void PWMPFC::SetOCRDutyCycle( unsigned long dc )
{
  if( dc > PWM_Max_DC ) SystemError();   // duty cycle out of bounds

  Set16BitPort( OCR, dc );               // set PWM duty cycle
}

/*
	Method Name: GetOCRDutyCycle
	Parameters: None
	Description: Returns a 16 bit representation of the duty cycle.  
*/


unsigned int PWMPFC::GetDutyCycle( void )
{
  unsigned long dc;

  dc = Get16BitPort( OCR );

  if( dc > PWM_Max_DC ) SystemError(); // duty cycle out of bounds

  // dc = 100 - (dc / (PWM_Max_DC/100));
  dc = 100 - (dc >> PWM_Shift_DC);

  return dc;
}

/*
	Method Name: SetOCRDutyCycle
	Parameters: A 16-bit unsigned integer representing the duty cycle.
	Description: Sets the duty cycle within the range [0 - 100]. If it's out of range,
	then it gives an error.  
*/

void PWMPFC::SetDutyCycle( unsigned int dc )
{
  unsigned long ocr_dc;

  // dc = PWM_Max_DC - (dc * (PWM_Max_DC/100));
  ocr_dc = PWM_Max_DC - (dc << PWM_Shift_DC);

  if( ocr_dc > PWM_Max_DC ) SystemError();   // duty cycle out of bounds

  Set16BitPort( OCR, ocr_dc );               // set PWM duty cycle
}

