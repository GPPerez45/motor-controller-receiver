/*
 * timer2oi.cpp
 * 
 * Timer 2 Class
 */

#include <avr/io.h>
#include <avr/interrupt.h>

#include "timer2oi.hpp"

/*
        Method Name:    GetTimer2IntFlag
        Parameters: None
        Description: Returns whether the interrupt flag is true or false in a 16 bit representation.
*/

unsigned int Timer2OI::GetTimer2IntFlag( void )
{
  return TMR2OI::TIMER2_INT;
}


/*
        Method Name: SetTimer2IntFlag
        Parameters: None
        Description: Enables the interrupt flag.
*/

void Timer2OI::SetTimer2IntFlag( void )
{
  TMR2OI::TIMER2_INT = true;
}

/*
        Method Name: ClrTimer2IntFlag
        Parameters: None
        Description: Sets the interrupt flag to false, effectively clearing it.
*/

void Timer2OI::ClrTimer2IntFlag( void )
{
  TMR2OI::TIMER2_INT = false;
}

ISR(TIMER2_OVF_vect)
{
  TMR2OI::TIMER2_CNT++;
  TCNT2 = TMR8BITOI::MILSEC_TIMER;  //     6 // Preload timer with precalculated value (1ms)
  TMR2OI::TIMER2_INT = true;
}

uint32_t millis( void )
{
  return TMR2OI::TIMER2_CNT;
}

/*
        Method Name: UpdateTimers
        Parameters: None
        Description: Decreases the timer value for each timer in the array.
*/

unsigned int Timer2OI::UpdateTimers( void )
{
  if( !GetTimer2IntFlag() ) return false;

  UpdateObjTimers();
  ClrTimer2IntFlag();

  for( unsigned int i = 0; i < TIMER2OI_ARRAY_SIZE; ++i )
    if( SysTimer[i] != 0 ) --SysTimer[i];

  return true;
}


/*
        Method Name: StartTimer
        Parameters: Enumeration Representing the timer number, a 16 bit integer representing the timer value.
        Description: Assigns the timer value to the specified timer number.
*/

void Timer2OI::StartTimer( const T2OI::t tnum, unsigned long tval )
{
  if( tnum >= TIMER2OI_ARRAY_SIZE ) SystemError();

  SysTimer[tnum] = tval;
}

/*
        Method Name: TimerDone
        Parameters: None
        Description: Returns a 16 bit integer boolean representing whether the timer is done
*/

unsigned int Timer2OI::TimerDone( const T2OI::t tnum )
{
  if( tnum >= TIMER2OI_ARRAY_SIZE ) SystemError();

  if( SysTimer[tnum] == 0 ) return true;
  return false;
}
