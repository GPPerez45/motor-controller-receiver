
/*
 * mcaidev.hpp
 *
 * Motor Controller Active Input Device
 *
 * Device variables to hold motor speeds and direction from active input device
 *
 * 
 *  
 */

#ifndef MCAIDEV_HPP_
#define MCAIDEV_HPP_

#include "RF24.h"
#include "syserror.hpp"

class MCAIDev : public SystemError
{
  private:
    static uint8_t const MAX_MSPEED = 100;
    static uint8_t const MAX_MDIR = 4;

  private:
    uint8_t LAST_MDIR;       // Motor Direction
    uint8_t LAST_M1SPEED;    // Left Motor
    uint8_t LAST_M2SPEED;    // Right Motor

  private:
    uint8_t MTRSPDDIR[ 4 ];

  public:
    uint8_t MDIR;       // Motor Direction
    uint8_t M1SPEED;    // Left Motor
    uint8_t M2SPEED;    // Right Motor
    uint8_t KSWITCH;    // Kill Switch

  public:
    MCAIDev()
    {
      M1SPEED = 0;           // Left Motor Speed
      M2SPEED = 0;           // Right Motor Speed
      MDIR    = 0;           // Motor Direction (Dead Zone)
      LAST_M1SPEED = 0;      // Last Left Motor Speed
      LAST_M2SPEED = 0;      // Last Right Motor Speed
      LAST_MDIR    = 0;      // Last Motor Direction (Dead Zone)
      KSWITCH = false;       // Kill Switch off
    };

    uint8_t Driver( class RF24 * );

    uint8_t GetM1Speed( void );
    void SetM1Speed( uint8_t  );

    uint8_t GetM2Speed( void );
    void SetM2Speed( uint8_t );

    uint8_t GetMDir( void );
    void SetMDir( uint8_t  );

    uint8_t SpeedChange( void );
    uint8_t DirectionChange( void );

    void ClearKillSwitch( void );
    void SetKillSwitch( void );
    uint8_t IsKillSwitchActive( void );

    void ClearMtrSpdDir( void );
};

#endif // MCAIDEV_HPP_

