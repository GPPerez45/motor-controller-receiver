/*
 * mc.hpp
 * Motor Controller
 *
 */

#ifndef MC_HPP_
#define MC_HPP_

#include "pmmio.hpp"
#include "pwmpfc.hpp"
#include "timerbase.hpp"
#include "objtimers.hpp"


class MotorController : public ObjTimers, public PMMIO
{
  private:
    class PWMPFC *M_PWMPTR;

    static uint8_t const M_STOPPED   = 0x00;    // Motor Speed Status
    static uint8_t const M_STARTING  = 0x01;
    static uint8_t const M_STOPPING  = 0x02;
    static uint8_t const M_FORWARD   = 0x03;
    static uint8_t const M_REVERSE   = 0x04;

    static uint8_t const M_OPEN      = 0x00;    // Motor Relay Status
    static uint8_t const M_CLOSED    = 0x01;
    static uint8_t const M_SWITCHING = 0x02;

    static uint8_t const M_RMP_TMR   = 0x00;    // Motor timer index
    static uint8_t const M_STP_TMR   = 0x01;
    static uint8_t const M_CLOS_TMR  = 0x02;
    static uint8_t const M_OPEN_TMR  = 0x03;

    static uint16_t const M_MIN_MRR  = 0x01;    // Minimum Motor Ramp Rate > 0

    uint8_t const M_DIR_OPEN;     // Motor direction relay open
    uint8_t const M_DIR_FWD;      // Motor direction relay forward
    uint8_t const M_DIR_REV;      // Motor direction relay reverse
    uint8_t const M_DIR_ERROR;    // Motor direction error
    uint8_t const M_DIR_MASK;     // Motor direction mask
    volatile uint8_t * M_DIR_PORT;     // Motor direction port pointer

    uint8_t const M_STRT_SPD;     // Motor Minimum Start Speed (5-30)
    uint8_t const M_MAX_SPD;      // Maximum Motor Speed
    uint8_t const M_MIN_SPD;      // Minimum Motor Speed

    uint16_t const M_STP_DUR;     // Motor Stop Duration (100-1000) (.1 sec - 1 sec)
    uint16_t const M_OPEN_DUR;    // Motor Relay Open Duration (1-100) (1 milisec - .1 sec)
    uint16_t const M_CLOS_DUR;    // Motor Relay Closed Duration (1-100) (1 milisec - .1 sec)

    uint8_t const M_BRK_ENABLED;  // Motor break enabled
    uint8_t const M_BRK_MASK;     // Motor break mask
    volatile uint8_t * M_BRK_PORT;     // Motor break port

    uint8_t M_ID_DMS;             // Motor Input Device Desired Motor Speed
    uint8_t M_ID_DMD;             // Motor Input Device Desired Motor Direction
    uint16_t M_ID_DMRR;           // Motor Input Device Desired Motor Ramp Rate

    uint8_t M_AMS;                // Actual Motor Speed (0-100)
    uint8_t M_DMS;                // Desired Motor Speed (0-100)

    uint8_t M_AMD;                // Actual Motor Direction
    uint8_t M_DMD;                // Desired Motor Direction

    uint16_t M_MRR;               // Motor Ramp Rate (MIN_MRR-1000)
    uint8_t M_MSS :3;             // Motor Speed Status -> M_STOPPED_, M_STARTING_,
    uint8_t M_MDS :2;             // Motor Direction Status -> M_OPEN_, M_CLOSE_,

    uint8_t M_KILLSWITCH :1;      // Kill Switch -> true, false

  private:
//    void SetAMS( uint8_t );

    void IncM_AMS( void );
    void DecM_AMS( void );

    void SetMtrPWM( void );
    void SetMtrSpeed( void );

    void SetAMSStatus( void );

    void SetM_MRR( uint16_t );

    void SetM_AMD( void );

    void RampMotor( void );

    void SetMDPins( void );
    void SetMDPort( void );

    void SetM_DMD( uint8_t );
    void SetDMDRmpDwn( void );

    void SetM_MDS( void );

    void SetMBPin( void );
    void ClrMBPin( void );

    void SetMtrBreak( void );

    void SyncMotor( void );

  public:
    MotorController( class TimerBase *tb_ptr, class PWMPFC *pwmptr,
                     volatile uint8_t * dir_port, volatile uint8_t * dir_ddr, volatile uint8_t * brk_port, volatile uint8_t * brk_ddr,
                     uint8_t const dir_open, uint8_t const dir_fwd, uint8_t const dir_rev, uint8_t const dir_mask,
                     uint8_t const strt_spd, uint8_t const max_spd, uint8_t const min_spd,
                     uint16_t const stp_dur, uint16_t const open_dur, uint16_t const clos_dur,
                     uint8_t const brk_enabled, uint8_t const brk_mask
                   ) : ObjTimers( tb_ptr, 4 ),
       M_DIR_OPEN(dir_open), M_DIR_FWD(dir_fwd), M_DIR_REV(dir_rev), M_DIR_ERROR(dir_mask), M_DIR_MASK(dir_mask),
       M_STRT_SPD(strt_spd), M_MAX_SPD(max_spd), M_MIN_SPD(min_spd),
       M_STP_DUR(stp_dur), M_OPEN_DUR(open_dur), M_CLOS_DUR(clos_dur),
       M_BRK_ENABLED(brk_enabled), M_BRK_MASK(brk_mask)
    {
      M_PWMPTR   = pwmptr;    // Pulse width modulation object instance pointer
      M_DIR_PORT = dir_port;  // Motor direction port pointer

      SetM_ID_DMS( 0 );       // Input Device Desired Motor Speed (0-100)
      SetM_ID_DMD( 0 );       // Input Device Desired Motor Direction
      SetM_ID_DMRR( 15 );     // Input Device Motor Ramp Rate (MIN_MRR-1000)

      M_AMS = 0;              // Actual Motor Speed (0-100) (AMS must start at 0)
      M_DMS = 0;              // Desired Motor Speed (0-100)
      M_DMD = 0;              // Desired Motor Direction
      M_AMD = 0;              // Actual Motor Direction
      SetM_MRR( 15 );         // Motor Ramp Rate (MIN_MRR-1000)

      M_MSS = 0;              // Motor Speed Status -> M_STOPPED_, M_STARTING_, ...
      M_MDS = 0;              // Motor Direction Status -> M_OPEN_, M_CLOSE_, ,...

      M_KILLSWITCH = 0;       // Kill Switch -> true, false

      SetPortBits( dir_ddr, M_DIR_MASK, M_DIR_MASK );    // Set DDR bits for motor direction
//      M_DIR_PORT = 0x00; //SetPortBits( dir_port, 0, M_DIR_MASK );
//      PORTC = 0x00;

      if( IsMBEnabled() )
      {
        M_BRK_PORT = brk_port;    // Motor break port pointer
        SetPortBits( brk_ddr, M_BRK_MASK, M_BRK_MASK );  // Set DDR bits for motor break
      }
//      else
//        M_BRK_PORT = 0x00;    // Motor break port pointer
    };

    //========= Motor Control Input Device Routines =======
    void SetM_ID_DMS( uint8_t );
    uint8_t GetM_ID_DMS( void );

    void SetM_ID_DMD( uint8_t );
    uint8_t GetM_ID_DMD( void );

    void SetM_ID_DMRR( uint16_t );
    uint16_t GetM_ID_DMRR( void );

    //============= Motor Speed Routines =================
    uint8_t GetM_MIN_SPD( void );
    uint8_t GetM_MAX_SPD( void );

    void SetM_DMS( uint8_t );
    uint8_t GetM_DMS( void );

    uint8_t GetM_AMS( void );
    int IsMStopped( void );

    //======= Motor Speed Status Routines ================
    uint8_t GetM_MSS( void );

    uint8_t GetM_STOPPED( void );    // Motor Speed Status
    uint8_t GetM_STARTING( void );
    uint8_t GetM_STOPPING( void );
    uint8_t GetM_FORWARD( void );
    uint8_t GetM_REVERSE( void );

    //=========== Motor Ramp Rate Routines ===============
    void SetMtrRampRate( void );

    uint8_t GetM_MIN_MRR( void );
    uint16_t GetM_MRR( void );

    //=========== Motor Direction Routines ===============
    uint8_t GetM_DIR_MASK( void );           // public

    uint8_t GetM_DIR_FWD( void );            // public
    uint8_t GetM_DIR_REV( void );            // public
    uint8_t GetM_DIR_OPEN( void );           // public
    uint8_t GetM_DIR_ERROR( void );          // public

    uint8_t GetMDPort( void );               // public
    uint8_t GetMDir( void );                 // public

    void ResetCloseDurTmr( void );
    void ResetStopDurTmr( void );

    //======== Motor Direction Status Routines =============
    uint8_t GetM_MDS( void );                // public

    uint8_t IsMDSOpen( void );               // public
    uint8_t IsMDSClosed( void );             // public

    //=========== Motor Break Routines ====================
    uint8_t IsMBOn( void );
    uint8_t IsMBOff( void );
    uint8_t IsMBEnabled( void );

    void SetMBOn( void );
    void SetMBOff( void );

    //=========== Motor Stop Routines ==============
    uint8_t IsDriveMotorStopped( void );
    void StopM( void );
    void SetKILLSWITCH( void );
    void ClrKILLSWITCH( void );
    void KillSwitchStop( void );

    //========== Main Driver Routines ==============
    void MCDriver( void );
};

#endif // MC_HPP_
