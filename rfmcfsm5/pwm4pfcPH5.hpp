/*
 * pwm4pfcPH5.hpp
 * Pulse Width Modulation Frequency and Phase Correct
 *
 */

#ifndef PWM4PFCPH5_HPP_
#define PWM4PFCPH5_HPP_

#include <avr/io.h>
#include "pwmpfc.hpp"
#include "pwm4pfc.hpp"

class PWM4PFCPH5 : public PWMPFC
{
  public:
    PWM4PFCPH5( void ) : PWMPFC( &OCR4C, PWM4::MAX_DC, PWM4::SHIFT_DC )
    {
      // Duty Cycle 0%
      SetDutyCycle( 0 );

      // Clear Timer Int Flag
      TIFR4 &= 0x01;

      // Set PWM Mode -> Phase & Frequency Correct (ie WGMn 1:0 = 0), non-inverted (COMnx1:0 = 2), inv (COMnx1:0 = 3)
      TCCR4A |= (0 << COM4A1) | (0 << COM4A0) | (0 << COM4B1) | (0 << COM4B0) | (1 << COM4C1) | (1 << COM4C0) |
                (0 << WGM41 ) | (0 << WGM40);

      // Set PWM Mode -> Phase & Frequency Correct (ie WGMn3 = 1, WGMn2 = 0)
      TCCR4B = (0 << ICNC4) | (0 << ICES4) | (0 << 5) | (1 << WGM43) | (0 << WGM42);

      // Set PWM Mode -> No Prescaler, Internal Clock
      TCCR4B |= PWM4::CS_PS;

      // Set PWM Frequency
      ICR4 = PWM4::ICR4_REG;

      // Set PWM PORT Bits as Outputs
      DDRH |= (1 << DDH5);

      // Clear Overflow Interrupt for Timer 3
      TIMSK4 &= 0x01;
    };
};

#endif /* PWM4PFCPH5_HPP_ */
