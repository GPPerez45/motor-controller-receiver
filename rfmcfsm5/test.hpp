/*
 * test.hpp
 *
 */


#ifndef TEST_HPP_
#define TEST_HPP_

#include "mc.hpp"
#include "timerbase.hpp"

void ExecController( class MotorController *, class Timer2OI * );

#endif // TEST_HPP_
