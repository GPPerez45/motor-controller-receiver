/*
 * timerbase.hpp
 * Base class for timers. 
 */

#include "timerbase.hpp"

TimerBase::TimerBase( void )
{
  for( unsigned int i=0; i < OBJPTR_SIZE; ++i )
    ObjPtr[i] = 0;
}

unsigned int TimerBase::AssignObject( class ObjTimers * o_ptr )
{
  volatile unsigned int i = 0;

  while( i < OBJPTR_SIZE )
  {
    if( ObjPtr[ i ] == 0 )
    {
      ObjPtr[ i ] = o_ptr;
      return true;
    }

    ++i;
  }

  SysError(0);
  return false;
}

unsigned int TimerBase::RemoveObject( class ObjTimers * o_ptr )
{
  for( unsigned int i=0; i < OBJPTR_SIZE; ++i )
  {
    if( ObjPtr[i] == o_ptr )
    {
      ObjPtr[i] = 0;
      return true;
    }
  }

  return false;
}

void TimerBase::UpdateObjTimers( void )
{
  volatile unsigned int i = 0;

  while( i < OBJPTR_SIZE )
  {
    if( ObjPtr[ i ] != 0 )
      ObjPtr[i]->UpdateTimers();

    ++i;
  }
}


