/*
 * pwm4pfc.hpp
 * Pulse Width Modulation Frequency and Phase Correct
 *

 */

#ifndef PWM4PFC_HPP_
#define PWM4PFC_HPP_

//-------------------------------------------------
// PWM Phase & Frequency Correct
//-------------------------------------------------
// ICRn Reg to define TOP (WGMn3:0 = 8)
// OCRnx Reg to generate PWM with OCn
// COMnx1:0 -> 2 for non-inverted, 3 for inverted
// DDR_OCnx set as output
//
// ATMega2560
// Fclk = 16MHz
// PWM Frequency = 20KHz
//
// The following formula came from Dean Camera
// avr freaks timer tutorial.
//
// TTC  = Target Timer Count
// TF   = Target Frequency
// TCF  = Timer Clock Frequency
// TCFP = Timer Clock Frequency Prescaler
//
// TTC  = ?
// TF   = 20KHz
// TCF  = 16Mhz
// TCFP = 1
//
// TTC = (1/TF) / (TCFP/TCF)
// TTC = (1/20000) / (1/16000000)
// TTC = .00005/.0000000625
// TTC = 800
//
// ICRn = 800/2
// ICRn = 400
//
// ---------------------------------
//

// Clock Select
//=================
// 0 No clock source. (Timer/Counter stopped)
// 1 Fclk 1    No Prescaling
// 2 Fclk 8    Prescaler
// 3 Fclk 64   Prescaler
// 4 Fclk 128  Prescaler
// 5 Fclk 1024 Prescaler
// 6 External clock source on Tn pin. Clock on falling edge
// 7 External clock source on Tn pin. Clock on rising edge
//
#define PWM_CS_NO_CLK_        0
#define PWM_CS_FCLK_NO_PS_    1    // _PS -> Prescale
#define PWM_CS_FCLK_8_PS_     2
#define PWM_CS_FCLK_64_PS_    3
#define PWM_CS_FCLK_256_PS_   4
#define PWM_CS_FCLK_1024_PS_  5
#define PWM_CS_EXT_CLK_FE_    6    // _FE -> Falling Edge
#define PWM_CS_EXT_CLK_RE_    7    // _RE -> Rising Edge


//=================================================================
// Defines for pwm structure of the motor controller
//
// NOTE: The following defines will eventually be specifed by a GUI
//=================================================================


//#define PWM4_TCF_  16000000         // Timer Clock Frequency 16MHz
//#define PWM4_TF_      20000         // Target Frequency 20KHz
//#define PWM4_TCFP_        1         // Timer Clock Frequency Prescaler 1
//#define PWM4_TTC_  ((1/PWM4_TF)/(PWM4_TCFP/PWM4_TCF)) // Target Timer Count
//#define PWM4_TTC_       800

//static unsigned long const PWM3_TTC = PWM3_TTC_;

//#define PWM4_ICR4_REG_  (PWM4_TTC >> 1)
//#define PWM4_MAX_DC_    (PWM4_TTC >> 1)
//#define PWM4_ICR4_REG_  400
//#define PWM4_MAX_DC_    400
//#define PWM4_SHIFT_DC_    2

namespace PWM4
{
  static unsigned long const ICR4_REG = 400;
  static unsigned long const MAX_DC = 400;
  static unsigned int const  SHIFT_DC = 2;
  static unsigned int const  CS_PS = PWM_CS_FCLK_NO_PS_;
}

//===== End Note =============

#endif // PWM4PFC_HPP_

