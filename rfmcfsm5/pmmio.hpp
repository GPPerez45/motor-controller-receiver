/*
 * pmmio.hpp
 *
 * Pointer to Memory Mapped IO port Routines
 *
 */

#ifndef PMMIO_HPP_
#define PMMIO_HPP_

#include <avr\io.h>

class PMMIO
{
  public:
    void SetPortBits( volatile uint8_t *, uint8_t, uint8_t );
    unsigned int GetPortBits( volatile uint8_t *, uint8_t );
    void Set16BitPort( volatile uint16_t *, uint16_t );
    unsigned long Get16BitPort( volatile uint16_t * );
};

#endif // PMMIO_HPP_
