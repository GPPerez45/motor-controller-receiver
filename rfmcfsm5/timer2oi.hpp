/*
 * timer2oi.hpp
 *
 */


#ifndef TIMER2OI_HPP_
#define TIMER2OI_HPP_

#include "timerbase.hpp"

//-----------------------------------------------------------
// Timer Normal Mode (8 bit, internal clk, prescaler 32, TOV)
//-----------------------------------------------------------
// ATMega128
// Fclk = 16MHz
// Timer Frequency = 1 ms
//
// TCCR2  = TRM2_NORM_INTCLK_CS64 = 0x03
// TIMSK |= TRM2_OVERFLOW_INT = 0x40
// TCNT2  = TMR8_MILSEC_TIMER = 0x03
//
// The folowing is from Dean Camera :
// AVR Freaks : Tutorial on Timers
//
// TTC  = Target Timer Count
// TF   = Target Frequency
// TCF  = Timer Clock Frequency
// TCFP = Timer Clock Frequency Prescaler
//
// TTC  = ?
// TF   = 1 ms
// TCF  = 16Mhz
// TCFP = 64
//
// TTC = (1/TF) / (TCFP/TCF) - 1
// TTC = (1/1000) / (64/16000000) - 1
// TTC = .001/.000004 - 1
// TTC = 249
//
// TMR8_MILSEC_TIMER_ = 255 - TCC
// TMR8_MILSEC_TIMER_ =   6
//
// ---------------------------------
//

namespace TMR2OI
{
  static uint32_t TIMER2_CNT = 0;
  static unsigned int TIMER2_INT = false;
}

namespace T2OI { enum t { T2OI0, T2OI1, T2OI2, T2OI3, T2OI4,
                          T2OI5, T2OI6, T2OI7, T2OI8, T2OI9 }; };


uint32_t millis( void );

//==========================================================================

class Timer2OI: public TimerBase      // 8 bit timer
{
  private:
    static unsigned int const NORM_INTCLK_CS64    = 0x00;
    static unsigned int const OVERFLOW_INT        = 0x01; //Modified
    static unsigned int const TIMER2OI_ARRAY_SIZE =   10;

    unsigned long SysTimer[ TIMER2OI_ARRAY_SIZE ];

  private:
    unsigned int GetTimer2IntFlag( void );
    void SetTimer2IntFlag( void );
    void ClrTimer2IntFlag( void );

  public:
    Timer2OI( void )
    {
      // Set Normal Mode, Internal Clk, Prescaler 64
      // TCCRA = (2 << FOC2) | (0 << WGM20) | (0 << COM21) | (0 << ) COM20) | (0 << WGM21) |
      //         (0 << CS22) | (1 << CS21) | (1 << CS20)
      TCCR2A = NORM_INTCLK_CS64;
          TCCR2B = 0x04;
      // Enable Overflow Interrupt
      // TIMSK = (0 << OCIE2) | (1 << TOIE2)
      TIMSK2 |= OVERFLOW_INT;

      // Preload timer with precalculated value (1ms), Fclk = 16MHz
      TCNT2 = TMR8BITOI::MILSEC_TIMER;

      TMR2OI::TIMER2_INT = false;
    };

    unsigned int UpdateTimers( void );

    void StartTimer( const T2OI::t, unsigned long );
    unsigned int TimerDone( const T2OI::t );
};

#endif // TIMER2OI_HPP_

