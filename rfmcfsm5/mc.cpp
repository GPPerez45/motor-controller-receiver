/*            `
* mc3.cpp
* Motor Controller
*
*/

#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "objtimers.hpp"
#include "syserror.hpp"
#include "pmmio.hpp"
#include "mc.hpp"


//=====================================================================
// Motor Control Input Device Routines
//=====================================================================

void MotorController::SetM_ID_DMS( uint8_t dms ) //Set Input device desired motor speed
{
  if( dms > M_MAX_SPD ) SystemError();    // Motor Speed must be in bounds (0-100)
  M_ID_DMS = dms;
}

uint8_t MotorController::GetM_ID_DMS( void ) //Get Input device desired motor speed
{
  if( M_ID_DMS > M_MAX_SPD ) SystemError();    // Motor Speed must be in bounds (0-100)

  return M_ID_DMS;
}

void MotorController::SetM_ID_DMD( uint8_t dmd ) //Set Input device desired motor dir.
{
  if( dmd > M_DIR_REV ) SystemError();    // Motor direction must be in bounds (0-2)

  M_ID_DMD = dmd;
}

uint8_t MotorController::GetM_ID_DMD( void ) //Get Input Device desired motor dir. 
{
  if( M_ID_DMD > M_DIR_REV ) SystemError();    // Motor direction must be in bounds (0-2)

  return M_ID_DMD;
}

void MotorController::SetM_ID_DMRR( uint16_t dmrr )
{
  if( dmrr < M_MIN_MRR ) SystemError();

  M_ID_DMRR = dmrr;
}

uint16_t MotorController::GetM_ID_DMRR( void )
{
  if( M_ID_DMRR < M_MIN_MRR ) SystemError();

  return M_ID_DMRR;
}


//=====================================================================
// Motor Speed Routines
//=====================================================================

uint8_t MotorController::GetM_MIN_SPD( void )  // public so we can the min speed from wherever
{
  if( M_MIN_SPD > M_MAX_SPD ) SystemError();
  return M_MIN_SPD;
}

uint8_t MotorController::GetM_MAX_SPD( void )  // public so we can get the max speed from wherever
{
  if( M_MIN_SPD > M_MAX_SPD ) SystemError();
  return M_MAX_SPD;
}

void MotorController::SetM_DMS( uint8_t dms ) //Set the desired motor speed
{
  if( dms > M_MAX_SPD ) SystemError();    // Motor Speed must be in bounds (0-100)
  M_DMS = dms;
}

uint8_t MotorController::GetM_DMS( void ) //Get the desired motor speed
{
  if( M_DMS > M_MAX_SPD ) SystemError();    // Motor Speed must be in bounds (0-100)
  return M_DMS;
}


uint8_t MotorController::GetM_AMS( void ) //Get the actual motor speed
{
  if( M_AMS > M_MAX_SPD ) SystemError();    // Motor Speed must be in bounds (0-100)
  return M_AMS;
}

void MotorController::IncM_AMS( void ) //Increase the motor speed.
{
  if( M_AMS > M_MAX_SPD ) SystemError();    // Motor Speed must be in bounds (0-100)
  if( M_AMS == M_MAX_SPD ) return;          // Motor Speed must be in bounds (0-100)

  M_AMS++;
}

void MotorController::DecM_AMS( void ) //Decrease the motor speed
{
  if( GetM_AMS() >  M_MAX_SPD ) SystemError();   // Motor Speed must be in bounds (0-100)
  if( GetM_AMS() == M_MIN_SPD ) return;          // Motor Speed must be in bounds (0-100)

  M_AMS--;
}

int MotorController::IsMStopped( void ) //Check if we hit min speed. 
{
  if( M_PWMPTR->GetDutyCycle() == M_MIN_SPD ) return true;

  return false;
}

void MotorController::SetMtrPWM( void ) //Set the PWM duty cycle based on the actual motor speed. 
{
  M_PWMPTR->SetDutyCycle( M_AMS );
}

void MotorController::SetMtrSpeed( void ) //Set the desired motor speed based on the input device desired motor speed (radio, joystick, etc.)
{
  if( IsMDSClosed() == true )
    SetM_DMS( GetM_ID_DMS() );
}


//=====================================================================
//  Motor Speed Status Routines
//=====================================================================

uint8_t MotorController::GetM_MSS( void ) //Get the status
{
  if( M_MSS > M_REVERSE ) SystemError();

  return M_MSS;
}

uint8_t MotorController::GetM_STOPPED( void ) //Stopped state
{
  return M_STOPPED;
}

uint8_t MotorController::GetM_STARTING( void ) //Starting state
{
  return M_STARTING;
}

uint8_t MotorController::GetM_STOPPING( void ) //Stopping state
{
  return M_STOPPING;
}

uint8_t MotorController::GetM_FORWARD( void ) //Forward state
{
  return M_FORWARD;
}

uint8_t MotorController::GetM_REVERSE( void ) //Reverse state
{
  return M_REVERSE;
}

void MotorController::SetAMSStatus( void ) //Sets the actual motor speed status. 
{
  uint8_t cmd;   // Current Motor Direction

  if( IsMStopped() == true )
  {
    if( TimerDone( M_STP_TMR ) == true )
    {
      M_MSS = M_STOPPED;
      return;
    }

    M_MSS = M_STOPPING;
  }
  else
  {
    StartTimer( M_STP_TMR, M_STP_DUR );

    if( IsMDSClosed() == true )
    {
      if( M_PWMPTR->GetDutyCycle() >= M_STRT_SPD )
      {
        cmd = GetMDir();

        if( cmd == M_DIR_FWD )
        {
          M_MSS = M_FORWARD;
        }
        else if( cmd == M_DIR_REV )
        {
          M_MSS = M_REVERSE;
        }
        else
          SystemError();
      }
      else
        M_MSS = M_STARTING;
    }
    else
      SystemError();
  }
}


//=====================================================================
//  Motor Ramp Rate Routines
//=====================================================================

uint8_t MotorController::GetM_MIN_MRR( void ) //get the ramp rate. 
{
  if( M_MIN_MRR == 0 ) SystemError(); // Minimum Motor Ramp Rate must be > 0

  return M_MIN_MRR;
}

void MotorController::SetM_MRR( uint16_t mrr )  // mrr user bounds in .1 sec (1-600), set it 
{
  if( mrr < M_MIN_MRR ) SystemError();   // Motor Ramp Rate must be in bounds >= 1, 1 = .1 sec from 0->100 speed
  M_MRR = mrr;
}

uint16_t MotorController::GetM_MRR( void ) //Get the ramp rate.
{
  if( M_MRR < M_MIN_MRR ) SystemError(); // Motor Ramp Rate must be in bounds >= 1, 1 = .1 sec from 0->100 speed
  return M_MRR;
}

void MotorController::SetMtrRampRate( void ) //Set the ramp rate based on what the input device wants. 
{
  SetM_MRR( GetM_ID_DMRR() ); 
}

void MotorController::RampMotor( void ) //Ramp the motor up and down. 
{
  if( GetM_AMS() != GetM_DMS() )
  {
    if( TimerDone( M_RMP_TMR ) == true )
    {
      if( GetM_AMS() < GetM_DMS() )
        IncM_AMS();
      else if( GetM_AMS() > GetM_DMS() )
        DecM_AMS();

      StartTimer( M_RMP_TMR, GetM_MRR() );
    }
  }
}


//=====================================================================
//  Motor Direction Routines
//=====================================================================

uint8_t MotorController::GetM_DIR_MASK( void )    // Motor direction port mask
{
  return M_DIR_MASK;
}

uint8_t MotorController::GetM_DIR_FWD( void )     // Motor direction forward pins
{
  return M_DIR_FWD;
}

uint8_t MotorController::GetM_DIR_REV( void )     // Motor direction reverse pins
{
  return M_DIR_REV;
}

uint8_t MotorController::GetM_DIR_OPEN( void )    // Motor direction open pins
{
  return M_DIR_OPEN;
}

uint8_t MotorController::GetM_DIR_ERROR( void )    // Motor direction open pins
{
  return M_DIR_ERROR;
}

uint8_t MotorController::GetMDPort( void ) //Get port values.
{
  return GetPortBits( M_DIR_PORT, 0xFF );
}

uint8_t MotorController::GetMDir( void ) //get direction
{
  uint8_t cmd;   // Current Motor Direction

  cmd = GetPortBits( M_DIR_PORT, M_DIR_MASK );
  if( cmd == GetM_DIR_ERROR() ) SystemError();

  return cmd;
}

void MotorController::SetMDPins( void ) //set the pins that we'll send to mc board
{
  uint8_t cmd;   // Current Motor Direction
  uint8_t mdir;  // Motor Direction

  mdir = M_AMD;

  if( mdir == M_DIR_ERROR ) SystemError();

  cmd = GetMDir();
  if( cmd != mdir &&
      cmd != M_DIR_OPEN &&
      mdir != M_DIR_OPEN )
    SystemError();

  SetPortBits( M_DIR_PORT, mdir, M_DIR_MASK );
}

void MotorController::SetMDPort( void ) //set port for direction
{
  SetMDPins();
}

void MotorController::SetM_AMD( void ) //set the actual motor direction
{
  if( M_MSS != M_STOPPED ) return;
  if( M_DMD == M_DIR_ERROR ) SystemError();

  M_AMD = M_DMD;
  return;
}

void MotorController::SetM_DMD( uint8_t dmd ) //set the desired motor direction. 
{
  if( dmd == M_DIR_ERROR ) SystemError();

  M_DMD = dmd;
  return;
}

void MotorController::SetDMDRmpDwn( void ) //Set the desired motor direction to ramp down.
{
  if( IsMDSClosed() == true )
  {
    if( GetMDir() != GetM_ID_DMD() )
    {
      SetM_DMS( M_MIN_SPD );
      SetM_MRR( GetM_ID_DMRR() );
      SetM_DMD( M_DIR_OPEN );
    }
    else
      SetM_DMD( GetM_ID_DMD() );
  }
  else if( IsMDSOpen() == true )
  {
    SetM_DMD( GetM_ID_DMD() );
  }
}


//=====================================================================
//  Motor Direction Status Routines
//=====================================================================

uint8_t MotorController::GetM_MDS( void ) //Get the motor dir status. 
{
  if( M_MDS > M_SWITCHING ) SystemError();

  return M_MDS;
}

uint8_t MotorController::IsMDSOpen( void ) //Is the dir open
{
  if( M_MDS == M_OPEN ) return true;
  return false;
}

uint8_t MotorController::IsMDSClosed( void ) //Is it closed?
{
  if( M_MDS == M_CLOSED ) return true;
  return false;
}

void MotorController::SetM_MDS( void ) //Set the status. 
{
  uint8_t mds;

  mds = GetMDir();
  if( mds == M_DIR_OPEN )
  {
    StartTimer( M_CLOS_TMR, M_CLOS_DUR );

    if( TimerDone( M_OPEN_TMR ) == true )
    {
      M_MDS = M_OPEN;
      return;
    }
  }
  else if( mds == M_DIR_FWD || mds == M_DIR_REV )
  {
    StartTimer( M_OPEN_TMR, M_OPEN_DUR );

    if( TimerDone( M_CLOS_TMR ) == true )
    {
      M_MDS = M_CLOSED;
      return;
    }
  }
  else
    SystemError();

  M_MDS = M_SWITCHING;
}


//=====================================================================
// Motor Stop Routines
//=====================================================================

uint8_t MotorController::IsDriveMotorStopped( void ) //Is is stopped?
{
  if( M_MSS == M_STOPPED ) return true;

  return false;
}

void MotorController::StopM( void ) //Stop the motor. 
{
  SetM_ID_DMS( M_MIN_SPD );
  SetM_ID_DMRR( M_MIN_MRR );
  SetM_ID_DMD( M_DIR_OPEN );

  SetM_DMS( M_MIN_SPD );
  SetM_MRR( M_MIN_MRR );
  SetM_DMD( M_DIR_OPEN );
}

void MotorController::SetKILLSWITCH( void ) //Enable kill switch
{
  M_KILLSWITCH = true;
}

void MotorController::ClrKILLSWITCH( void )  // private, disable kill switch. 
{
  M_KILLSWITCH = false;
}

void MotorController::KillSwitchStop( void )   // After a kill signal, wait for motors to stop 
{
  if( M_KILLSWITCH == true )
  {
    if( IsDriveMotorStopped() == true )
      ClrKILLSWITCH();
    else
      StopM();
  }
}

//=====================================================================
// Motor Brake Routines
//=====================================================================

uint8_t MotorController::IsMBOn( void ) //Brakes on?
{
  if( M_BRK_PORT == 0 || IsMBEnabled() == 0 ) return false;
  if( GetPortBits( M_BRK_PORT, M_BRK_MASK ) == M_BRK_MASK ) return true;
  return false;
}

uint8_t MotorController::IsMBOff( void ) //Brakes Off?
{
  if( M_BRK_PORT == 0 || IsMBEnabled() == 0 ) return true;
  if( GetPortBits( M_BRK_PORT, M_BRK_MASK ) == 0 ) return true;
  return false;
}

uint8_t MotorController::IsMBEnabled( void ) //Are the breaks enabled?
{
  if( M_BRK_ENABLED == 0 ) return false;
  return true;
}

void MotorController::SetMBPin( void ) //Set the pins. 
{
  if( M_BRK_PORT == 0 ) return;
  SetPortBits( M_BRK_PORT, M_BRK_MASK, M_BRK_MASK );
}

void MotorController::ClrMBPin( void ) //Clear the break pins. 
{
  if( M_BRK_PORT == 0 ) return;
  SetPortBits( M_BRK_PORT, 0, M_BRK_MASK );
}

void MotorController::SetMBOn( void ) //Set the break pins on. 
{
  if( IsMBEnabled() != 0 && IsMStopped() != 0 )
    SetMBPin();
  else
    ClrMBPin();
}

void MotorController::SetMBOff( void ) //Clear the breaks. 
{
  ClrMBPin();
}

void MotorController::SetMtrBreak( void ) //Set the motor breaks to on if enabled. 
{
  if( IsMBEnabled() )
    SetMBOn();
  else
    SetMBOff();
}


//=====================================================================
// Syncronize Motor Controller Timers
//=====================================================================

void MotorController::SyncMotor( void )
{
  if( M_AMS == M_DMS )          // update ramp timer
    StartTimer( M_RMP_TMR, M_MRR );

  if( M_AMS != M_MIN_SPD )      // update motor stopped timer
    StartTimer( M_STP_TMR, M_STP_DUR );

  if( GetMDir() != M_DIR_OPEN ) // update motor dir open timer
    StartTimer( M_OPEN_TMR, M_OPEN_DUR );

  if( GetMDir() == M_DIR_OPEN ) // update motor dir closed timer
    StartTimer( M_CLOS_TMR, M_CLOS_DUR );
}

void MotorController::ResetCloseDurTmr() //Reset the close timer. 
{
  StartTimer( M_CLOS_TMR, M_CLOS_DUR );
}


void MotorController::ResetStopDurTmr() //Reset the stop timer (*ISSUE*)
{
  StartTimer( M_STP_TMR, M_STP_DUR );
}


//=====================================================================
// Main Routines
//=====================================================================


void MotorController::MCDriver( void ) // public
{
  KillSwitchStop();        // Stops WC regardless of Active Input Device Input

  SetM_MDS();
  SetAMSStatus();

  SetMtrSpeed();
  SetMtrRampRate();

  SetDMDRmpDwn();

  SetM_AMD();

  SetMDPort();

  SetMtrBreak();

  RampMotor();
  SetMtrPWM();

  SyncMotor();
}


