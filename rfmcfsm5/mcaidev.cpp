
/*
 * mcaidev.cpp
 *
 * Motor Controller Active Input Device
 *
 * Device variables to hold motor speeds and direction
 *
 */

#include "mcaidev.hpp"

uint8_t MCAIDev::Driver( class RF24 * radio )
{
  uint8_t kswitch;

  if( radio->available() )
  {
    // Fetch the data payload
    radio->read( MTRSPDDIR, sizeof( MTRSPDDIR ) );

    SetM1Speed( MTRSPDDIR[0] );
    SetM2Speed( MTRSPDDIR[1] );
    SetMDir( MTRSPDDIR[2] );

    if( M1SPEED > MAX_MSPEED || M2SPEED > MAX_MSPEED ) SysError( 0 );
    if( MDIR > MAX_MDIR ) SysError( 0 );

    kswitch = MTRSPDDIR[3];
    if( kswitch != 0 )
    {
      SetKillSwitch();
    }

    return true;
  }

  return false;
}

uint8_t MCAIDev::GetM1Speed( void )             // Left Motor Speed
{
  return M1SPEED;
}

void MCAIDev::SetM1Speed( uint8_t mspd )       // Left Motor Speed
{
  if( mspd > MAX_MSPEED ) SysError( 0 );

  LAST_M1SPEED = M1SPEED;
  M1SPEED = mspd;
}

uint8_t MCAIDev::GetM2Speed( void )             // Right Motor Speed
{
  return M2SPEED;
}

void MCAIDev::SetM2Speed( uint8_t mspd )       // Right Motor Speed
{
  if( mspd > MAX_MSPEED ) SysError( 0 );

  LAST_M2SPEED = M2SPEED;
  M2SPEED = mspd;
}

uint8_t MCAIDev::GetMDir( void )               // Motor Direction
{
  return MDIR;
}

void MCAIDev::SetMDir( uint8_t mdir )          // Motor Direction
{
  if( mdir > MAX_MDIR ) SysError( 0 );

  LAST_MDIR = MDIR;
  MDIR = mdir;
}

uint8_t MCAIDev::SpeedChange( void )
{
  if( M1SPEED > MAX_MSPEED || M2SPEED > MAX_MSPEED ) SysError( 0 );

  if( M1SPEED == 0 && M2SPEED == 0 ) return true;
  if( M1SPEED != LAST_M1SPEED ) return true;
  if( M2SPEED != LAST_M2SPEED ) return true;

  return false;
}

uint8_t MCAIDev::DirectionChange( void )
{
  if( MDIR > MAX_MDIR ) SysError( 0 );

  if( MDIR == 0 ) return true;
  if( MDIR != LAST_MDIR ) return true;

  return false;
}

void MCAIDev::ClearKillSwitch( void )           { KSWITCH = false; };
void MCAIDev::SetKillSwitch( void )             { KSWITCH = true; };
uint8_t MCAIDev::IsKillSwitchActive( void )     { return KSWITCH; };

void MCAIDev::ClearMtrSpdDir( void )
{
  M1SPEED = 0;           // Left Motor Speed
  M2SPEED = 0;           // Right Motor Speed
  MDIR    = 0;           // Motor Direction (Dead Zone)
  LAST_M1SPEED = 0;      // Last Left Motor Speed
  LAST_M2SPEED = 0;      // Last Right Motor Speed
  LAST_MDIR    = 0;      // Last Motor Direction (Dead Zone)
  KSWITCH = true;        // Kill Switch ON
}

