/*
 * jsmcfsm5.c
 * Joystick finite state machine
 */


#include <avr/io.h>
#include <avr/interrupt.h>

#include "mc.hpp"
//#include "rfjsdev.h"
#include "mcaidev.hpp"
#include "jsmcfsm5.hpp"
#include "syserror.hpp"


void JSMCFSM5::SyncMotors( class MotorController *mc1, class MotorController *mc2 )
{

  if( mc1->IsMStopped() && !mc2->IsMStopped() )
  {
    mc1->ResetStopDurTmr();
    mc2->ResetStopDurTmr();
  }
  else if( mc2->IsMStopped() && !mc1->IsMStopped() )
  {
    mc1->ResetStopDurTmr();
    mc2->ResetStopDurTmr();
  }




//  if( mc1->GetM_MSS() != mc1->GetM_STOPPING() && mc2->GetM_MSS() == mc2->GetM_STOPPING() )
//    mc2->ResetStopDurTmr();
//  else if( mc2->GetM_MSS() != mc2->GetM_STOPPING() && mc1->GetM_MSS() == mc1->GetM_STOPPING() )
//    mc1->ResetStopDurTmr();

//  if( mc1->GetM_MSS() == mc1->GetM_STOPPING() && mc2->GetM_MSS() == mc2->GetM_STOPPED() )
//  {
//    mc2->ResetOpenDurTmr();
//    mc1->ResetOpenDurTmr();
//  }
//  else if( mc2->GetM_MSS() == mc2->GetM_STOPPING() && mc1->GetM_MSS() == mc1->GetM_STOPPED() )
//  {
//    mc1->ResetOpenDurTmr();
//    mc2->ResetOpenDurTmr();
//  }

//  if( mc1->GetMDir() == mc1->GetM_DIR_OPEN() && mc2->GetMDir() != mc2->GetM_DIR_OPEN() )
//  {
//    mc1->ResetStopDurTmr();
//    mc2->ResetStopDurTmr();
//  }
//  else if( mc2->GetMDir() == mc2->GetM_DIR_OPEN() && mc1->GetMDir() != mc1->GetM_DIR_OPEN() )
//  {
//    mc1->ResetStopDurTmr();
//    mc2->ResetStopDurTmr();
//  }
}

uint8_t JSMCFSM5::IsMSSStopped( class MotorController *mc1, class MotorController *mc2 )
{
  if( mc1->GetM_MSS() == mc1->GetM_STOPPED() && mc2->GetM_MSS() == mc2->GetM_STOPPED() )
    return true;

  return false;
}

uint8_t JSMCFSM5::GetMtrDir( class MotorController *mc1, class MotorController *mc2 )
{
  uint8_t dir;
  dir = mc1->GetMDir() | mc2->GetMDir();
  return dir;
}

void JSMCFSM5::SetMtrRampRate( class MotorController *mc1,  class MotorController *mc2, uint8_t rr )
{
//  if( mc1->GetM_AMS() <= MSS && mc2->GetM_AMS() <= MSS )
//  {
//    mc1->SetM_ID_DMRR( FR_SRR );
//    mc2->SetM_ID_DMRR( FR_SRR );
//  }
//  else
//  {
    mc1->SetM_ID_DMRR( rr );
    mc2->SetM_ID_DMRR( rr );
//  }
}

void JSMCFSM5::SetMtrRampDown( class MotorController *mc )
{
  mc->SetM_ID_DMS( 0 );
  mc->SetM_ID_DMRR( FR_RDR );
  mc->SetM_ID_DMD( mc->GetM_DIR_OPEN() );
}

void JSMCFSM5::SetMotorsRampDown( class MotorController *mc1, class MotorController *mc2 )
{
  SetMtrRampDown( mc1 );
  SetMtrRampDown( mc2 );
  SyncMotors( mc1, mc2 );
}

void JSMCFSM5::SetMtrRev( class MotorController *mc1, class MotorController *mc2, class MCAIDev *js )
{
  SetMtrRampRate( mc1, mc2, FR_RUR );
  mc1->SetM_ID_DMS( js->GetM1Speed() );
  mc2->SetM_ID_DMS( js->GetM2Speed() );

  mc1->SetM_ID_DMD( mc1->GetM_DIR_REV() );
  mc2->SetM_ID_DMD( mc2->GetM_DIR_REV() );
}

void JSMCFSM5::SetMtrFPLftTurn( class MotorController *mc1, class MotorController *mc2, class MCAIDev *js )
{
  SetMtrRampRate( mc1, mc2, FP_RTR );
  mc1->SetM_ID_DMS( js->GetM1Speed() );
  mc2->SetM_ID_DMS( js->GetM2Speed() );

  mc1->SetM_ID_DMD( mc1->GetM_DIR_REV() );
  mc2->SetM_ID_DMD( mc2->GetM_DIR_FWD() );
}

void JSMCFSM5::SetMtrFPRgtTurn( class MotorController *mc1, class MotorController *mc2, class MCAIDev *js )
{
  SetMtrRampRate( mc1, mc2, FP_RTR );
  mc1->SetM_ID_DMS( js->GetM1Speed() );
  mc2->SetM_ID_DMS( js->GetM2Speed() );

  mc1->SetM_ID_DMD( mc1->GetM_DIR_FWD() );
  mc2->SetM_ID_DMD( mc2->GetM_DIR_REV() );
}

void JSMCFSM5::SetMtrFwd( class MotorController *mc1, class MotorController *mc2, class MCAIDev *js )
{
  SetMtrRampRate( mc1, mc2, FR_RUR );
  mc1->SetM_ID_DMS( js->GetM1Speed() );
  mc2->SetM_ID_DMS( js->GetM2Speed() );

  mc1->SetM_ID_DMD( mc1->GetM_DIR_FWD() );
  mc2->SetM_ID_DMD( mc2->GetM_DIR_FWD() );
}


//================= Joystick Finite State Machine ==================

void JSMCFSM5::MachState0( class MotorController *mc1, class MotorController *mc2, class MCAIDev *js )  // JS throw dead zone
{
  switch( GetMtrDir( mc1, mc2 ) )
  {
    case MTR_DIR_FWD :
    case MTR_DIR_REV :
    case MTR_DIR_LEFT  :
    case MTR_DIR_RIGHT :
    case MTR_DIR_OPEN  :
    case MTR1_DIR_REV :
    case MTR2_DIR_REV :
    case MTR1_DIR_FWD :
    case MTR2_DIR_FWD :
      SetMotorsRampDown( mc1, mc2 );
      break;

    default :
      SystemError();
  }
}

void JSMCFSM5::MachState1( class MotorController *mc1, class MotorController *mc2, class MCAIDev *js )  // JS throw right
{
  switch( GetMtrDir( mc1, mc2 ) )
  {
    case MTR_DIR_RIGHT :
    case MTR_DIR_OPEN  :
      SetMtrFPRgtTurn( mc1, mc2, js );
      break;

    case MTR_DIR_FWD :
    case MTR_DIR_REV :
    case MTR_DIR_LEFT :
    case MTR1_DIR_REV :
    case MTR2_DIR_REV :
    case MTR1_DIR_FWD :
    case MTR2_DIR_FWD :
      SetMotorsRampDown( mc1, mc2 );
      break;

    default :
      SystemError();
  }
}

void JSMCFSM5::MachState2( class MotorController *mc1, class MotorController *mc2, class MCAIDev *js )  // JS throw left
{
  switch( GetMtrDir( mc1, mc2 ) )
  {
    case MTR_DIR_LEFT  :
    case MTR_DIR_OPEN  :
      SetMtrFPLftTurn( mc1, mc2, js );
      break;

    case MTR_DIR_FWD :
    case MTR_DIR_REV :
    case MTR_DIR_RIGHT :
    case MTR1_DIR_REV :
    case MTR2_DIR_REV :
    case MTR1_DIR_FWD :
    case MTR2_DIR_FWD :
      SetMotorsRampDown( mc1, mc2 );
      break;

    default :
      SystemError();
  }
}

void JSMCFSM5::MachState3( class MotorController *mc1, class MotorController *mc2, class MCAIDev *js )  // JS throw backwards
{
  switch( GetMtrDir( mc1, mc2 ) )
  {
    case MTR_DIR_REV  :
    case MTR_DIR_OPEN  :
      SetMtrRev( mc1, mc2, js );
      break;

    case MTR_DIR_FWD :
    case MTR_DIR_LEFT  :
    case MTR_DIR_RIGHT :
    case MTR1_DIR_REV :
    case MTR2_DIR_REV :
    case MTR1_DIR_FWD :
    case MTR2_DIR_FWD :
      SetMotorsRampDown( mc1, mc2 );
      break;

    default :
      SystemError();
  }
}


void JSMCFSM5::MachState4( class MotorController *mc1, class MotorController *mc2, class MCAIDev *js )  // JS throw forewards
{
  switch( GetMtrDir( mc1, mc2 ) )
  {
    case MTR_DIR_FWD  :
    case MTR_DIR_OPEN  :
      SetMtrFwd( mc1, mc2, js );
      break;

    case MTR_DIR_REV :
    case MTR_DIR_RIGHT :
    case MTR_DIR_LEFT  :
    case MTR1_DIR_REV :
    case MTR2_DIR_REV :
    case MTR1_DIR_FWD :
    case MTR2_DIR_FWD :
      SetMotorsRampDown( mc1, mc2 );
      break;

    default :
      SystemError();
  }
}


void JSMCFSM5::FSM5State( class MotorController *mc1, class MotorController *mc2, class MCAIDev *js )
{
  switch( js->GetMDir() )
  {
      // JS throw dead zone
    case DZ :      // Dead zone
      MachState0( mc1, mc2, js );
      break;

      // JS throw right
    case FPRT :    // Fixed position right turn
      MachState1( mc1, mc2, js );
      break;

      // JS throw left
    case FPLT :    // Fixed position left turn
      MachState2( mc1, mc2, js );
      break;

      // JS throw backwards
    case REV :
      MachState3( mc1, mc2, js );
      break;

      // JS throw forewards
    case FWD :
      MachState4( mc1, mc2, js );
      break;

    default:
      SystemError();
  }
}


void JSMCFSM5::ExecFSM5State( class MotorController *mc1, class MotorController *mc2, class MCAIDev *js )
{
  FSM5State( mc1, mc2, js );
}
