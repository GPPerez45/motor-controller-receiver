/* 
    MC1.hpp
	
	
	Motor Controller 1 Implementation
*/
#include <avr/io.h>

#include "mc.hpp"
#include "timerbase.hpp"
#include "pwmpfc.hpp"
class MC1 : public MotorController
{
  public:
    MC1( class TimerBase *tb_ptr, class PWMPFC *pwmptr ) :
           MotorController( tb_ptr, pwmptr,   // timer pointer class, pwm pointer class
              &PORTC, &DDRC, &PORTD, &DDRD,   // M_DIR_PORT, M_DIR_PORT DDR, M_BRK_PORT, M_BRK_PORT DDR
              0x00, 0x01, 0x02, 0x03,  // M_DIR_OPEN, M_DIR_FWD, M_DIR_REV, M_DIR_ERROR, M_DIR_MASK
              1, 100, 0,              // M_STRT_SPD, M_MAX_SPD, M_MIN_SPD
              750, 100, 100,           // M_STP_DUR, M_OPEN_DUR, M_CLOS_DUR
              0x01, 0x01 )             // M_BRK_ENABLED, M_BRK_MASK
  {}
};

