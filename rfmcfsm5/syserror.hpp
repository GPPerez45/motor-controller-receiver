/*
 * syserror.hpp
 * Infinite loop error state header file. 
 */


#ifndef SYSERROR_HPP_
#define SYSERROR_HPP_

class SystemError
{
  public:
    void SysError( unsigned int );
};

#endif /* SYSERROR_HPP_ */
