/*
 * objtimers.hpp
 *
 *
 */


#ifndef OBJTIMERS_HPP_
#define OBJTIMERS_HPP_

#include "syserror.hpp"

class TimerBase;

// This class is to be inherited into the object requiring timers

class ObjTimers: public SystemError
{
  private:
    unsigned int TimerNum;
    unsigned long * ObjectTimers;
    class TimerBase * TimerBasePtr;

  protected:
    void StartTimer( unsigned int const, unsigned long );
    unsigned int TimerDone( unsigned int const );

  public:
    ObjTimers( class TimerBase *, unsigned int const );
    ~ObjTimers( void );

    void UpdateTimers( void );
};

#endif // OBJTIMERS_HPP_

